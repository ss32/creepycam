import cv2
import os
import platform
from subprocess import Popen, PIPE
import sys
import time
import zmq
import yaml
import numpy as np



opencv_python_version = lambda str_version: tuple(map(int, (str_version.split("."))))
assert opencv_python_version(cv2.__version__) >= opencv_python_version("4.10.0"), \
       "Please install latest opencv-python for benchmark: python3 -m pip install --upgrade opencv-python"


# See https://github.com/opencv/opencv_zoo/blob/main/models/face_detection_yunet/demo.py
from yunet import YuNet
# Valid combinations of backends and targets
backend_target_pairs = [
    [cv2.dnn.DNN_BACKEND_OPENCV, cv2.dnn.DNN_TARGET_CPU],
    [cv2.dnn.DNN_BACKEND_CUDA,   cv2.dnn.DNN_TARGET_CUDA],
    [cv2.dnn.DNN_BACKEND_CUDA,   cv2.dnn.DNN_TARGET_CUDA_FP16],
    [cv2.dnn.DNN_BACKEND_TIMVX,  cv2.dnn.DNN_TARGET_NPU],
    [cv2.dnn.DNN_BACKEND_CANN,   cv2.dnn.DNN_TARGET_NPU]
]




class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


if platform.release().find("tegra") != -1:
    try:
        import jetson.inference
        import jetson.utils

        jetson_inference = True
    except ModuleNotFoundError:
        print(
            bcolors.WARNING
            + "WARNING: Jetson inference utility not found, continuing with OpenCV only."
            + bcolors.ENDC
        )


def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config


def zmq_setup(config):
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    host_addr = "tcp://{}:{}".format(config["server"], config["port"])
    socket.bind(host_addr)
    print("Successful bind at {}".format(host_addr))
    return socket


def get_face_detect_model(config):
    model = YuNet(modelPath='face_detection_yunet_2023mar.onnx',
                  inputSize=[config["capture_width"], config["capture_height"]],
                  confThreshold=config["face_threshold"],
                  nmsThreshold=config["nms_threshold"],
                  topK=config["topK"],
                  backendId=cv2.dnn.DNN_BACKEND_OPENCV,
                  targetId=cv2.dnn.DNN_TARGET_CPU)
    return model


def find_people(frame, net, previous_coords):
    img = jetson.utils.cudaFromNumpy(frame[:, :, ::-1])
    detections = net.Detect(img, overlay="none")
    people = []
    x, y, w, h = previous_coords
    for i, d in enumerate(detections):
        if d.ClassID == 1:
            w = int(d.Width)
            h = int(d.Height)
            x = int(d.Left)
            y = int(d.Top)
            people.append([x, y, w, h])
    if len(people) > 0:
        person_index = find_largest_rectangle(people)
        x, y, w, h = people[person_index]
        detection_time = time.time()
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 225, 0), 2)
    return frame, x, y, w, h, detection_time


def find_faces(frame, model, previous_coords, previous_detection_time):
    person_x, person_y, rect_w, rect_h = previous_coords
    detection_time = previous_detection_time
    try:
        faces = model.infer(frame)
    except:
        faces = []

    # If more than one face is found, focus on the largest (closest) one
    if len(faces) != 0:
        detection_time = time.time()
        if len(faces) > 1:
            
            face_index = find_largest_rectangle(faces)
            faces = [faces[face_index]]
        
        person_x, person_y, rect_w, rect_h = faces[0][0:4].astype(np.int32)
        cv2.rectangle(
            frame,
            (person_x, person_y),
            (person_x + rect_w, person_y + rect_h),
            (0, 0, 255),
            2,
        )
    return frame, person_x, person_y, rect_w, rect_h, detection_time


def find_largest_rectangle(faces):
    max_area = 0
    for face_index, f in enumerate(faces):
        current_area = f[2] * f[3]
        if current_area > max_area:
            max_area = current_area
            biggest_face = face_index
    return biggest_face


def gstreamer_pipeline(config):
    return (
        "nvarguscamerasrc ! "
        "video/x-raw(memory:NVMM), "
        "width={}, height={}, "
        "format=(string)NV12, framerate={}/1 ! "
        "nvvidconv flip-method={} ! "
        "video/x-raw, width={}, height={}, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink".format(
            config["capture_width"],
            config["capture_height"],
            config["framerate"],
            config["rotate_image"],
            config["display_width"],
            config["display_height"],
        )
    )


def main():
    config = get_config()
    socket = zmq_setup(config)

    # Check to make sure server and client are the same machine
    if config["lauch_arduino_process"]:
        start_arduino = True
        if not (config["server"] == "127.0.0.1" and config["client"] == "localhost"):
            print(
                bcolors.WARNING
                + "WARNING: Cannot start Arduino process if client and host are not on the same machine.\n"
                + "Set server to 127.0.0.1 and client to localhost in order to use this feature."
                + bcolors.ENDC
            )
            start_arduino = False
        if start_arduino:
            process = Popen(["python3", "arduino_control.py"], stdout=PIPE, stderr=PIPE)

    # Video saving logic
    save_video = config["save_full_video"] or config["save_people_video"]

    if save_video:
        if config["raspicam"]:
            framerate = config["framerate"]
        else:
            framerate = 30
        video_file_path = os.path.join(
            config["save_path"], "{}.mp4".format(int(time.time()))
        )
        print("Saving video to {}".format(video_file_path))
        video_codec = cv2.VideoWriter_fourcc(*"mp4v")
        video_out = cv2.VideoWriter(
            video_file_path,
            video_codec,
            framerate,
            (config["capture_width"], config["capture_height"]),
        )

    model = get_face_detect_model(config)
    # Create network for person detection
    if config["detect_people"]:
        if jetson_inference:
            net = jetson.inference.detectNet(threshold=config["person_threshold"])

    # Check to see if we're using the Pi cam, if not make sure the device path is present
    if config["raspicam"]:
        video_capture = cv2.VideoCapture(gstreamer_pipeline(config), cv2.CAP_GSTREAMER)
        if config["verbose"]:
            print(gstreamer_pipeline(config))
    else:
        if not config["camera_path"]:
            print(
                "\nERROR: You're trying to use a USB webcam without setting the device path!"
            )
            print(
                "Check config.yaml and either set raspicam: True or define the camera device path."
            )
            sys.exit(0)
        video_capture = cv2.VideoCapture(config["camera_path"])

    # Placeholder values in case no face is found in the image feed
    previous_coords = [0, 0, 50, 50]
    previous_detection_time = 0

    # Print video stats if in verbose mode
    if config["verbose"]:
        print(
            "Capture W x H: {} x {}".format(
                config["capture_width"], config["capture_height"]
            )
        )
        print("Framerate: {}fps".format(config["framerate"]))
        if config["show_image"]:
            print(
                "Display W x H: {} x {}".format(
                    config["display_width"], config["display_height"]
                )
            )

    # Main vision routine
    while video_capture.isOpened():
        try:
            # Capture a frame
            ret, frame = video_capture.read()

            if ret:
                img_dims = frame.shape

                # Person detection
                if config["detect_people"]:
                    (
                        frame,
                        person_x,
                        person_y,
                        rect_w,
                        rect_h,
                        detection_time,
                    ) = find_people(frame, net, previous_coords)
                    previous_coords = [person_x, person_y, rect_w, rect_h]

                # Face detection
                if config["detect_faces"]:
                    # (frame, person_x, person_y, rect_w, rect_h, detection_time) = face_detect.detect_faces(frame,config,face_net,previous_coords)
                    (
                        frame,
                        person_x,
                        person_y,
                        rect_w,
                        rect_h,
                        detection_time,
                    ) = find_faces(
                        frame, model, previous_coords, previous_detection_time
                    )
                dt = detection_time  - previous_detection_time
                previous_detection_time = detection_time
                # Write a video frame if we're saving a video
                save_detection_frame = config["save_people_video"] and (
                    time.time() - detection_time < 5
                )

                if save_detection_frame or config["save_full_video"]:
                    video_out.write(frame)

                # Send information about the person in the frame to the arduino control device
                socket.send_string(config["servo_topic"], zmq.SNDMORE)
                socket.send_pyobj(
                    [person_x, person_y, rect_w, rect_h, img_dims, detection_time]
                )
                previous_coords = [person_x, person_y, rect_w, rect_h]

                if config["verbose"]:
                    print("Face [x,y]: [{},{}]".format(person_x, person_y))
                    print("Detection time: {}".format(detection_time))
                    print("Processing time: {}".format(dt))
                    print("Rectangle dimensions: {} x {}".format(rect_w, rect_h))

                if config["show_image"]:
                    cv2.imshow("Camera Feed", frame)
                    cv2.waitKey(1)

            else:
                print("Couldn't get frame")

        except KeyboardInterrupt:
            print("Got kill signal, terminating Arduino process...")
            break

    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()
    if save_video:
        video_out.release()
    socket.send_string("killsig", zmq.SNDMORE)
    socket.send_pyobj(True)
    print("Exiting")
    time.sleep(1)
    sys.exit(0)


if __name__ == "__main__":
    main()
