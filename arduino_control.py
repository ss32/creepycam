from pyfirmata import SERVO
import pyfirmata
import sys
from time import sleep
import time
import yaml
import zmq
import random

TOP_REST = 105
BOTTOM_REST = 90
X_REST = 90
def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config


def blink(top_eyelid_pin, top_eyelid_position):
    
    # Close
    top_eyelid_pin.write(TOP_REST)
    sleep(0.5)
    # Open
    top_eyelid_pin.write(top_eyelid_position)

    sleep(0.25)

def zmq_setup(config):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    client_addr = "tcp://{}:{}".format(config["client"], config["port"])
    socket.connect(client_addr)
    socket.setsockopt(zmq.SUBSCRIBE, "{}".format(config["servo_topic"]).encode())
    socket.setsockopt(zmq.SUBSCRIBE, "killsig".encode())
    print("Successful ZMQ subscription at {}".format(client_addr))
    return socket


def arduino_setup(config):
    board = pyfirmata.Arduino("{}".format(config["board_path"]))
    print("Found Arduino at {}".format(config["board_path"]))
    # Start an iterator thread so the serial buffer doesn't overflow
    iter8 = pyfirmata.util.Iterator(board)
    iter8.start()

    # Define control pins
    xpin = board.get_pin("d:{}:s".format(config["xpin"]))
    ypin = board.get_pin("d:{}:s".format(config["ypin"]))
    top_eyelid_pin = board.get_pin("d:{}:s".format(config["top_eyelid_pin"]))
    bottom_eyelid_pin = board.get_pin("d:{}:s".format(config["bottom_eyelid_pin"]))

    if config["verbose"]:
        print("Setting up servo pins.")
        print(
            "Eye x-axis: {}, Eye y-axis: {}, Bottom eyelid: {}, Top eyelid {}".format(
                config["xpin"],
                config["ypin"],
                config["top_eyelid_pin"],
                config["bottom_eyelid_pin"],
            )
        )

    # Set servos to their middle position so nothing binds or collides
    ypin.write(90)
    xpin.write(X_REST)
    top_eyelid_pin.write(TOP_REST)
    bottom_eyelid_pin.write(BOTTOM_REST)

    return xpin, ypin, top_eyelid_pin, bottom_eyelid_pin


def map_to_range(value, x1, x2, y1, y2):
    xRange = x2 - x1
    yRange = y2 - y1
    remapped_value = float(value - x1) / float(xRange)
    return int(y1 + (remapped_value * yRange))


def get_servo_positions(packet_from_camera, config, suspicious=False):
    global eyeOpen
    # Unpack the data from the network and translate the image coordinates to servo coorindates
    face_x, face_y, rect_w, rect_h, img_dims, detection_time = packet_from_camera
    eye_x_position = (
        map_to_range(
            face_x + (rect_w / 2.0), 0, img_dims[1], config["XMAX"], config["XMIN"]
        )
        - config["XOFFSET"]
    )
    eye_y_position = map_to_range(
        face_y + (rect_h / 2.0), 0, img_dims[0], config["YMAX"], config["YMIN"]
    )
    top_eyelid_position = (
        map_to_range(
            face_y + (rect_h / 2.0),
            0,
            img_dims[0],
            config["TOP_MAX"],
            config["TOP_MIN"],
        )
        + config["OPEN_FACTOR"]
    )
    bottom_eyelid_position = 180 - (
        map_to_range(
            face_y + (rect_h / 2.0),
            0,
            img_dims[0],
            config["BOTTOM_MAX"],
            config["BOTTOM_MIN"],
        )
        - config["OPEN_FACTOR"]
    )
    if suspicious:
        top_eyelid_position -= 10
        bottom_eyelid_position -= 20
    # Ensure that nothing moves more than it should
    if bottom_eyelid_position > config["BOTTOM_MAX"]:
        bottom_eyelid_position = config["BOTTOM_MAX"]

    if bottom_eyelid_position < config["BOTTOM_MIN"]:
        bottom_eyelid_position = config["BOTTOM_MIN"]

    if top_eyelid_position > config["TOP_MAX"]:
        top_eyelid_position = config["TOP_MAX"]

    if top_eyelid_position < config["TOP_MIN"]:
        top_eyelid_position = config["TOP_MIN"]

    if time.time() - detection_time > config["sleep_time"]:
        eye_x_position = 90
        eye_y_position = 90
        top_eyelid_position = TOP_REST
        bottom_eyelid_position = BOTTOM_REST
        print(f"Sleeping at {time.time()}")
        

    return eye_x_position, eye_y_position, top_eyelid_position, bottom_eyelid_position


def main():
    config = get_config()
    socket = zmq_setup(config)
    xpin, ypin, top_eyelid_pin, bottom_eyelid_pin = arduino_setup(config)
    tBlink = config["BLINK_MIN_TIME_SECONDS"] + random.randint(0, config["BLINK_MIN_TIME_SECONDS"])
    tStart = time.time()
    tSus = time.time()
    tSusTimer = random.randint(0,config["SUSPICIOUS_TIME_SECONDS"])
    suspicious = False
    while True:

        # Only blink if we're looking at someone
        if time.time() - tStart > tBlink and top_eyelid_position != TOP_REST:
            blink(top_eyelid_pin, top_eyelid_position)
            tBlink = config["BLINK_MIN_TIME_SECONDS"] + random.randint(0, config["BLINK_MIN_TIME_SECONDS"])
            tStart = time.time()        
        if time.time() - tSus > tSusTimer:
            tSus = time.time()
            tSusTimer = random.randint(0,config["SUSPICIOUS_TIME_SECONDS"])
            suspicious = not suspicious
        topic = socket.recv_string()
        if topic == "servo":
            data = socket.recv_pyobj()
            (
                eye_x_position,
                eye_y_position,
                top_eyelid_position,
                bottom_eyelid_position,
            ) = get_servo_positions(data, config, suspicious)
            if config["verbose"]:
                print(
                    "Servo positions: x: {} y: {} top eyelid: {}, bottom eyelid: {}".format(
                        eye_x_position,
                        eye_y_position,
                        top_eyelid_position,
                        bottom_eyelid_position,
                    )
                )
            xpin.write(eye_x_position)
            ypin.write(eye_y_position)
            bottom_eyelid_pin.write(bottom_eyelid_position)
            top_eyelid_pin.write(top_eyelid_position)
           

        if topic == "killsig":
            data = socket.recv_pyobj()
            if data:
                print("Got kill signal from camera, exiting")
                sleep(0.25)
                ypin.write(90)
                xpin.write(X_REST)
                top_eyelid_pin.write(TOP_REST)
                bottom_eyelid_pin.write(BOTTOM_REST)
                sys.exit(0)


if __name__ == "__main__":
    main()
