# Creepy Cam


<div style="text-align:center"><img src="./eyebot.gif" /></div>

Multiple servo + camera combination that looks for people and/or faces and follows them around.  Intended as a Halloween decoration but could also work for a security camera that has no respect for personal space.   OpenCV is used for face detection, PyTorch via the [Jetson-Inference](https://github.com/dusty-nv/jetson-inference) library does person detection, and [ZMQ](https://zeromq.org/) is used for communication between the main vision pipeline and the Arduino control script. 

My [servo tool](https://www.gitlab.com/ss32/servotool) came out of this project as well.

## About

Based on [this](https://www.instructables.com/3D-Printed-Animatronic-Eye-Mechanism-on-the-Cheap/) joystick-controlled 3D printed eyeball, this is a scaled-up and "smarter" take used as a Halloween decoration.  The eyeball is 250% larger and instead of using a joystick to control the movement, OpenCV and PyTorch are used to detect faces and people and follow them in the frame.  The project is built around the [Jetson Nano](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-nano/) 4GB version, but might work on the new 2GB version. and the architecture of the project allows inference to be executed on a remote machine, so in that setup a Raspberry Pi could be used to control the Arduino while the heavy lifting is done by something more powerful on the network.  

## Design + Hardware

Face and person detection happens in the [vision pipeline routine](./vision_pipieline.py), and the servos are controlled through an Arduino connected to the Nano via [PyFirmata](https://pypi.org/project/pyFirmata/) in the [arduino control script](./arduino_control.py). 

The default configuration assumes that both scripts are running on the same machine, but messages are passed over TCP using the ZMQ library, so in theory the vision pipeline could be exectued on a machine with a GPU while something less powerful on the network controls the Arduino.

The camera used here is a PiCam due to serial issues when using a USB webcam while controlling the Arduino over USB, but the GStreamer function utilized in the vision pipeline will work with a USB camera as well.


STL files are provided for the modified eyeball, eyelids, and base, as well as a servo "booster seat" that moves the X-axis servo up to avoid colliding with the Y axis servo.  The universal joint originally came from [Thingiverse](https://www.thingiverse.com/thing:219070) but I resized it and reworked the toggle to be more reliable.  All parts except the U-joint and servo horn extensions were printed with supports.

Servo horn extenders are used on both axes of the eyball.  The dual 20mm extender is for the X axis, and 10mm extender for the Y axis.

## Installation

Install Python requirements with 

```bash
python3 -m pip install -r requirements.txt --user
```
and if you want to run person detection, follow the install guide for Jetson Inference on their repo.  I recommend [building from source](https://github.com/dusty-nv/jetson-inference/blob/master/docs/building-repo-2.md) to remove the small overhead of running in a Docker container. 

The Arduino software comes bundled with the Arduino IDE and can be found under `File-->Examples-->Firmata-->StandardFirmata`.

## Hardware Setup

![hardware](hardware.jpg)

Electrical components are straightforward.  The external 5V supply is critical to operation.  Trying to drive the servos from the Arduino will result in either no movement or the Arduino browning out and restarting.

![eyeball](eyeball_behind.jpeg)

Very janky testing setup with a good view of the mechanics. Paperclips are cut and bent to shape to control the various axes.  A single servo drives the y-axis despite there being two attachment points on the eyeball itself.  Fitting can be a little tedious but the imprecise tolerances on the eyeball axes coupled with the u-joint result in more natural movement, especially in the x-axis.

See the [config file](./config.yaml) for all configuration options, but by default the eyeball X-Y axes are on pins 9 and 8, respectively, the top eyelid servo is on pin 7, and the bottom eyelid is on pin 6.  Connect the Arduino to the Jetson or PC over USB **and also supply 5V to the Arudino from the Jetson's GPIO pins or a 5VDC supply** .  Without the additional 5V input the servos will not have enough power to move.

### Note on Powering the Jetson

 If powered over USB, the Jetson cannot supply enough power to run the vision pipeline and all of the servos.  A 5V 4A (20W) power supply is required to power the it using the barrel connector.  I bought [this 25W power supply](https://amzn.to/34QavNE).  When powering the Jetson through the barrel connector, short J48 on the dev board using a jumper to allow the board to power up using that connection.

## Running

Either script can be started first, but generally I will start the vision pipeline first, followed by the Arduino control in a separate window.  If both scripts are running on the same machine you can set `lauch_arduino_process` to  `True` in the config file to automatically start the Arduino script in the background when the vision pipeline starts.  All configuration options are described in the config file.

### Limitations
The Nano is able run reliably at 15fps when doing face detection *or* person detection, but not both.  Future work will focus on speeding up both of these functions and possibly running them in parallel instead of the current serial execution.


